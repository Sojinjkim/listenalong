import React, { useState, useCallback, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";

import {
    Button,
    Grid,
    Typography,
} from '@mui/material';

import CreateRoomPage from "./CreateRoomPage";
import MusicPlayer from './MusicPlayer'


const Room = ({ clearRoomCode }) => {
    const [votesToSkip, setVotesToSkip] = useState(2)
    const [guestCanPause, setGuestCanPause] = useState(true)
    const [isHost, setIsHost] = useState(false)
    const [showSettings, setShowSettings] = useState(false)
    const [spotifyAuthenticated, setSpotifyAuthenticated] = useState(false)
    const [song, setSong] = useState({})

    let { roomCode } = useParams();

    const navigate = useNavigate();

    const updateShowSettings = (value) => {
        setShowSettings(value)
    }

    const authenticateSpotify = () => {
        fetch('/spotify/is-authenticated')
            .then((response) => response.json())
            .then((data) => {
                setSpotifyAuthenticated(data.status);
                if (!data.status) {
                    fetch('/spotify/get-auth-url')
                        .then((response) => response.json())
                        .then((data) => {
                            window.location.replace(data.url);
                        })
                }
        })
    }

    const getRoomDetails = () => {
        fetch(`/api/get-room?code=${roomCode}`)
            .then((response) => {
                if (!response.ok) {
                    clearRoomCode();
                    navigate('/');
                }
                return response.json()
            })
            .then((data) => {
                setVotesToSkip(data.votes_to_skip)
                setGuestCanPause(data.guest_can_pause)
                setIsHost(data.is_host)
                if (isHost) {
                    authenticateSpotify();
                }
            });
        }

    getRoomDetails()

    const getCurrentSong = () => {
        fetch('/spotify/current-song')
        .then((response) => {
            if (!response.ok) {
                return {}
            } else {
                return response.json();
            }
        })
        .then((data) => {
            setSong(data)
            console.log(data)
        })
    }


    const leaveButtonPressed = () => {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
        };
        fetch('/api/leave-room', requestOptions)
        .then((_response) => {
            clearRoomCode();
            navigate('/')
        })
    }

    const renderSettingsButton = () => {
        return (
            <Grid item xs={12} align={'center'}>
                <Button
                    variant={'contained'}
                    color='primary'
                    onClick={() => updateShowSettings(true)}
                >
                    Settings
                </Button>
            </Grid>
        );
    }


    const updateRoomDetails = useCallback(() => {
        return getRoomDetails()
    }, [votesToSkip, guestCanPause, isHost])

    useEffect(() => {
        const interval = setInterval(getCurrentSong, 1000)

        return function clearPoll() {
            clearInterval(interval)
        }
    })

    const renderSettings = () => {
        return (
            <Grid container spacing={1}>
                <Grid item xs={12} align='center'>
                    <CreateRoomPage
                        update={true}
                        votesToSkip={votesToSkip}
                        guestCanPause={guestCanPause}
                        roomCode={roomCode}
                        updateRoomDetails={updateRoomDetails}
                    />
                </Grid>
                <Grid item xs={12} align='center'>
                    <Button
                        variant={'contained'}
                        color='secondary'
                        onClick={() => updateShowSettings(false)}
                    >
                        Close
                    </Button>
                </Grid>
            </Grid>
        )
    }

    if (showSettings) {
        return renderSettings()
    } else {
        return (
            <>
                <Grid container spacing={1}>
                    <Grid item xs={12} align='center'>
                        <Typography variant='h4' component='h4'>
                            Code: {roomCode}
                        </Typography>
                    </Grid>
                    <MusicPlayer song={song} />
                    {/* <Grid item xs={12} align='center'>
                        <Typography variant='h6' component='h6'>
                            Votes: {votesToSkip}
                        </Typography>
                    </Grid>
                    <Grid item xs={12} align='center'>
                        <Typography variant='h6' component='h6'>
                            Guest Can Pause: {guestCanPause.toString()}
                        </Typography>
                    </Grid>
                    <Grid item xs={12} align='center'>
                        <Typography variant='h6' component='h6'>
                            Host: {isHost.toString()}
                        </Typography>
                    </Grid> */}
                    {isHost ? renderSettingsButton() : null}
                    <Grid item xs={12} align='center'>
                        <Button variant='contained' color='secondary' onClick={leaveButtonPressed}>
                            Leave Room
                        </Button>
                    </Grid>
                </Grid>
            </>
        );
    }
}

export default Room;
