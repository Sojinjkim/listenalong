import React, { Component, useState, useEffect, useCallback } from "react";
import { render } from "react-dom";
import * as ReactDOMClient from 'react-dom/client';

import HomePage from "./HomePage";
import CreateRoomPage from "./CreateRoomPage";
import JoinRoomPage from "./JoinRoomPage";
import Room from "./Room"
import Info from "./Info"

import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    Redirect,
    Navigate,
} from "react-router-dom";

function App() {
    const [roomCode, setRoomCode] = useState(null)

    useEffect(() => {
        fetch('/api/user-in-room')
        .then((response) => response.json())
        .then((data) => {
            console.log()
            setRoomCode(data.code)
        })
    })

    const clearRoomCode = useCallback(() => {
        setRoomCode(null)
    }, [roomCode])


    return (
        <div className='center'>
            <Router>
                <Routes>
                    <Route exact path='/' element={roomCode ? <Navigate replace to={`/room/${roomCode}`} /> : <HomePage />} />
                    <Route exact path='/join' element={<JoinRoomPage />} />
                    <Route exact path='/info' element={<Info />} />
                    <Route exact path='/create' element={<CreateRoomPage />} />
                    <Route exact path='/room/:roomCode' element={<Room clearRoomCode={clearRoomCode}/>} />
                </Routes>
            </Router>
        </div>
    
    )
}

export default App;

const container = document.getElementById('app');

const root = ReactDOMClient.createRoot(container);
root.render(<App tab="App" />)