import React, { useState } from 'react';
import {
    Grid,
    Button,
    Typography,
    IconButton
} from '@mui/material';
import {
    NavigateBefore,
    NavigateNext,
} from '@mui/icons-material'
import { Link } from "react-router-dom";


const pages = {
    JOIN: 'pages.join',
    CREATE: 'pages.create'
}


const Info = (props) => {
    const [page, setPage] = useState(pages.JOIN)

    const joinInfo = () => {
        return 'Join page'
    }

    const createInfo = () => {
        return 'Create page'
    }

    return (
        <Grid container spacing={1}>
            <Grid item xs={12} align='center'>
                <Typography component='h4' variant='h4'>
                    What is Listen Along?
                </Typography>
            </Grid>
            <Grid item xs={12} align='center'>
                <Typography variant='body1'>
                    { page == pages.JOIN ? joinInfo() : createInfo() }
                </Typography>
            </Grid>
            <Grid item xs={12} align='center'>
                <IconButton
                    onClick={() => {
                        page === pages.CREATE ? setPage(pages.JOIN) : setPage(pages.CREATE)
                    }}>
                        { page === pages.CREATE ? <NavigateBefore /> : <NavigateNext /> }
                </IconButton>
            </Grid>
            <Grid item xs={12} align='center'>
                <Button color='secondary' variant='contained' to='/' component={Link}>
                    Back
                </Button>
            </Grid>
        </Grid>
    )
}

export default Info;