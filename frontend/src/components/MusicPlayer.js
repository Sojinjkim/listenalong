import React, { Component, useState } from "react";
import {
    Grid,
    Typography,
    Card,
    IconButton,
    LinearProgress,
} from '@mui/material';
import {
    PlayArrow,
    SkipNext,
    Pause,
 } from '@mui/icons-material'

const MusicPlayer = (props) => {
    const pauseSong = () => {
        const requestOptions = {
            method: "PUT",
            headers: {"Content-Type": "application/json"}
        };
        fetch("/spotify/pause", requestOptions)
    }

    const playSong = () => {
        const requestOptions = {
            method: "PUT",
            headers: {"Content-Type": "application/json"}
        };
        fetch("/spotify/play", requestOptions)
    }

    const skipSong = () => {
        const requestOptions = {
            method: "POST",
            headers: {"Content-Type": "application/json"}
        };
        fetch("/spotify/skip", requestOptions)
    }

    const songProgress = props.song.time / props.song.duration * 100

    return (
        <Card>
            <Grid container alignItems='center'>
                <Grid item align='center' xs={4}>
                    <img src={props.song.image_url} height='100%' width='100%' />
                </Grid>
                <Grid item align='center' xs={8}>
                    <Typography component='h5' variant='h5'>
                        {props.song.title}
                    </Typography>
                    <Typography color='textSecondary' variant='subtitle1'>
                        {props.song.artist}
                    </Typography>
                    <div>
                        <IconButton>
                            {props.song.is_playing ? <Pause onClick={pauseSong}/> : <PlayArrow onClick={playSong}/>}
                        </IconButton>
                        <IconButton>
                            <SkipNext onClick={skipSong}/>
                            {props.song.votes} / {props.song.votes_required}
                        </IconButton>
                    </div>
                </Grid>
            </Grid>
            <LinearProgress color='primary' variant='determinate' value={songProgress} />
        </Card>
    )
}

export default MusicPlayer;